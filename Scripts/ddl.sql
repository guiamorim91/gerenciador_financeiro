-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema gerenciador_financeiro
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `gerenciador_financeiro` ;

-- -----------------------------------------------------
-- Schema gerenciador_financeiro
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `gerenciador_financeiro` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `gerenciador_financeiro` ;

-- -----------------------------------------------------
-- Table `usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `usuario` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `usuario` (
  `co_usuario` INT NOT NULL AUTO_INCREMENT,
  `no_usuario` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`co_usuario`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `tipo_conta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tipo_conta` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tipo_conta` (
  `co_tipo_conta` INT NOT NULL AUTO_INCREMENT,
  `ds_tipo_conta` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`co_tipo_conta`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `conta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `conta` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `conta` (
  `co_conta` INT NOT NULL AUTO_INCREMENT,
  `ds_conta` VARCHAR(100) NOT NULL,
  `co_usuario` INT NOT NULL,
  `co_tipo_conta` INT NOT NULL,
  PRIMARY KEY (`co_conta`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `categoria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `categoria` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `categoria` (
  `co_categoria` INT NOT NULL AUTO_INCREMENT,
  `ds_categoria` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`co_categoria`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `tipo_pagamento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tipo_pagamento` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tipo_pagamento` (
  `co_tipo_pagamento` INT NOT NULL AUTO_INCREMENT,
  `ds_tipo_pagamento` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`co_tipo_pagamento`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `receita`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `receita` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `receita` (
  `co_receita` INT NOT NULL AUTO_INCREMENT,
  `ds_receita` VARCHAR(100) NOT NULL,
  `vl_receita` DECIMAL(15,2) NOT NULL,
  `dt_receita` DATE NOT NULL,
  `co_categoria` INT NOT NULL,
  `co_conta` INT NOT NULL,
  `ds_observacao` VARCHAR(45) NULL,
  `st_receita_fixa` TINYINT(1) NULL DEFAULT 0,
  `st_pago` TINYINT(1) NULL DEFAULT 1,
  `co_tipo_pagamento` INT NOT NULL,
  PRIMARY KEY (`co_receita`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `despesa`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `despesa` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `despesa` (
  `co_despesa` INT NOT NULL AUTO_INCREMENT,
  `ds_despesa` VARCHAR(100) NOT NULL,
  `vl_despesa` DECIMAL(15,2) NOT NULL,
  `dt_despesa` DATE NOT NULL,
  `co_categoria` INT NOT NULL,
  `co_conta` INT NOT NULL,
  `ds_observacao` VARCHAR(45) NULL,
  `st_despesa_fixa` TINYINT(1) NULL DEFAULT 0,
  `st_pago` TINYINT(1) NULL DEFAULT 1,
  `co_tipo_pagamento` INT NOT NULL,
  PRIMARY KEY (`co_despesa`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `tipo_meta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tipo_meta` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tipo_meta` (
  `co_tipo_meta` INT NOT NULL AUTO_INCREMENT,
  `ds_tipo_meta` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`co_tipo_meta`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `meta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `meta` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `meta` (
  `co_meta` INT NOT NULL AUTO_INCREMENT,
  `ds_meta` VARCHAR(100) NOT NULL,
  `co_tipo_meta` INT NOT NULL,
  `co_categoria` INT NOT NULL,
  `co_usuario` INT NOT NULL,
  `vl_meta` DECIMAL(15,2) NOT NULL,
  PRIMARY KEY (`co_meta`))
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
