-- Media mensal de gastos do usuario
SELECT format(avg(soma),2,'de_DE') as media
FROM (SELECT sum(d.vl_despesa) as soma
      FROM despesa d
        INNER JOIN conta c USING (co_conta)
        INNER JOIN usuario u USING (co_usuario)
      WHERE u.co_usuario = 1
      GROUP BY month(d.dt_despesa)) AS tb_soma;

-- Media mensal de gastos do usuario por categoria
SELECT format(avg(soma),2,'de_DE') as media
FROM (SELECT sum(d.vl_despesa) as soma
      FROM despesa d
        INNER JOIN conta c USING (co_conta)
        INNER JOIN usuario u USING (co_usuario)
        INNER JOIN categoria ca USING (co_categoria)
      WHERE u.co_usuario = 1 AND ca.co_categoria = 7
      GROUP BY month(d.dt_despesa)) AS tb_soma;