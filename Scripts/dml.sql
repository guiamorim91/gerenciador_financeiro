-- CARGA USUARIO
insert INTO usuario (no_usuario) VALUE ('Maria');
insert INTO usuario (no_usuario) VALUE ('Joao');

-- CARGA CATEGORIA
INSERT INTO categoria (ds_categoria) VALUE ('Alimentação');
INSERT INTO categoria (ds_categoria) VALUE ('Carro');
INSERT INTO categoria (ds_categoria) VALUE ('Cartão de credito');
INSERT INTO categoria (ds_categoria) VALUE ('Entretenimento');
INSERT INTO categoria (ds_categoria) VALUE ('Estudo');
INSERT INTO categoria (ds_categoria) VALUE ('Lazer');
INSERT INTO categoria (ds_categoria) VALUE ('Moradia');
INSERT INTO categoria (ds_categoria) VALUE ('Transporte');
INSERT INTO categoria (ds_categoria) VALUE ('Salario');
INSERT INTO categoria (ds_categoria) VALUE ('Imovel');
INSERT INTO categoria (ds_categoria) VALUE ('Pensao');

-- CARGA TIPO CONTA
INSERT INTO tipo_conta (ds_tipo_conta) VALUE ('Conta corrente');
INSERT INTO tipo_conta (ds_tipo_conta) VALUE ('Conta poupança');
INSERT INTO tipo_conta (ds_tipo_conta) VALUE ('Conta salario');
INSERT INTO tipo_conta (ds_tipo_conta) VALUE ('Cartão de credito');

-- CARGA TIPO PAGAMENTO
INSERT INTO tipo_pagamento (ds_tipo_pagamento) VALUE ('Boleto');
INSERT INTO tipo_pagamento (ds_tipo_pagamento) VALUE ('Cartão');
INSERT INTO tipo_pagamento (ds_tipo_pagamento) VALUE ('Dinheiro');
INSERT INTO tipo_pagamento (ds_tipo_pagamento) VALUE ('Deposito');

-- CARGA CONTA
INSERT INTO conta (ds_conta, co_tipo_conta, co_usuario) VALUES ('Itau', 1, 1);
INSERT INTO conta (ds_conta, co_tipo_conta, co_usuario) VALUES ('Nubank', 4, 1);
INSERT INTO conta (ds_conta, co_tipo_conta, co_usuario) VALUES ('Bradesco', 2, 2);

-- CARGA RECEITA
INSERT INTO receita (ds_receita, co_categoria, co_conta, co_tipo_pagamento, st_pago, dt_receita, ds_observacao, vl_receita) VALUES ('Salario liquido', 9, 1, 4, 1, '2016-6-01', 'Salario mensal', 1755.25);
INSERT INTO receita (ds_receita, co_categoria, co_conta, co_tipo_pagamento, st_pago, dt_receita, ds_observacao, vl_receita) VALUES ('Aluguel', 10, 1, 3, 0, '2016-6-05', 'Aluguel', 800.00);
INSERT INTO receita (ds_receita, co_categoria, co_conta, co_tipo_pagamento, st_pago, dt_receita, ds_observacao, vl_receita) VALUES ('Pensao alimenticia', 11, 3, 3, 1, '2016-6-10', 'Pensao alimenticia etc', 720.50);

-- CARGA DESPESAS


-- Usuario Maria
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Churrascaria Porcão', '102', '2016-04-06', '1', '1', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Burguer King', '31.90', '2016-04-20', '1', '1', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Pamonha Pura', '7.89', '2016-05-17', '1', '1', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Giraffas', '21.90', '2016-05-19', '1', '1', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Subway', '12.90', '2016-05-20', '1', '1', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Livro Engenharia Software I Novatec', '79.90', '2016-01-25', '5', '2', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Livro UML Novatec', '32.90', '2016-01-30', '5', '2', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Livro SCRUM Ciência Moderna', '44.90', '2016-01-30', '5', '2', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Pneu Pirelli', '249.90', '2016-05-11', '2', '1', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Gasolina', '50', '2016-05-22', '2', '1', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Troca de Óleo', '61.29', '2016-05-19', '2', '1', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Luz Maio', '141.87', '2016-06-10', '7', '1', 'Sem obs', '0', '1', '1');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Água Maio', '55.23', '2016-06-10', '7', '1', 'Sem obs', '0', '1', '1');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Gás', '58', '2016-06-09', '7', '1', 'Sem obs', '0', '1', '1');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Churrascaria Porcão', '102', '2016-05-06', '1', '2', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Churrascaria Porcão', '102', '2016-06-06', '1', '2', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Luz Junho', '100.15', '2016-07-10', '7', '1', 'Sem obs', '0', '1', '1');


-- Usuario João
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Mc Donalds', '32.65', '2016-04-01', '1', '3', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Chiquinho sorvetes', '31.90', '2016-04-20', '1', '3', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Dogão do zé', '7.89', '2016-05-17', '1', '3', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Morte Lenta', '21.90', '2016-05-19', '1', '3', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Subway', '12.90', '2016-05-20', '1', '3', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('SOLID - Casa do codigo', '79.90', '2016-01-25', '5', '3', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Orientação a objetos do jeito certo - Casa do Codigo', '32.90', '2016-01-30', '5', '3', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Livro SCRUM Ciência Moderna', '44.90', '2016-01-30', '5', '3', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Pneu Pirelli', '249.90', '2016-05-11', '2', '3', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Gasolina', '560', '2016-05-12', '2', '3', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Troca de Óleo', '61.29', '2016-05-17', '2', '3', 'Sem obs', '0', '1', '2');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Luz Maio', '141.87', '2016-06-11', '7', '3', 'Sem obs', '0', '1', '1');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Água Maio', '55.23', '2016-06-14', '7', '3', 'Sem obs', '0', '1', '1');
INSERT INTO despesa (ds_despesa, vl_despesa, dt_despesa, co_categoria, co_conta, ds_observacao, st_despesa_fixa, st_pago, co_tipo_pagamento) VALUES ('Gás', '58', '2016-06-09', '7', '3', 'Sem obs', '0', '1', '1');


-- Carga de Tipo de Meta
INSERT INTO tipo_meta (ds_tipo_meta) VALUES ('Receita');
INSERT INTO tipo_meta (ds_tipo_meta) VALUES ('Despesa');

-- Carga da tabela de Metas
INSERT INTO meta (ds_meta, co_tipo_meta, co_categoria, co_usuario, vl_meta) VALUES ('Refeição', 2, 1, 1, 450.50);
INSERT INTO meta (ds_meta, co_tipo_meta, co_categoria, co_usuario, vl_meta) VALUES ('Vale-Transporte', 2, 8, 1, 450.50);
